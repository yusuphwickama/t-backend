<?php
/**
 * Created by PhpStorm.
 * User: Wickerman
 * Date: 2/7/2017
 * Time: 10:51 PM
 */

ini_set('display_errors', 'Off');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $apikey = urldecode($_POST["apiKey"]);
    $localAPIKey = file_get_contents("files/api_key_app");

    if ($apikey == $localAPIKey) {
        require_once 'auth/generate.php';
        require_once 'auth/db-connect.php';

        $id = getToken(6);
        $phoneID = getToken(6);
        $oneID = getToken(6);
        $twoID = getToken(6);
        
        // TABLE : user_details
        $altTwo = '';
        $fName = urldecode($_POST["fName"]);
        $altOne = urldecode($_POST["altOne"]);
        $altTwo = urldecode($_POST["altTwo"]);
        if (strlen($altTwo) < 5) {
            $altTwo = null;
        }

        // TABLE : phone_details
        $main = urldecode($_POST["phoneNum"]);

        // TABLE : avatar_details
        $image64 = '';

        $image64 = $_POST["image"];

        // Turn off auto-commit (increase performance)
        $con->autocommit(false);

        /* BEGIN ADDING PHONE NUMBERS */

        $addPhone = $con->prepare("INSERT INTO phone_numbers (ph_id, number) VALUES (?,?) ON DUPLICATE KEY UPDATE phone_numbers.number=values(number)");
        $addPhone->bind_param('ss', $phoneID, $phoneNum);

        // Add main number
        $phoneID = getToken(6);
        $phoneNum = $main;
        if (!$addPhone->execute()) {
            $error = $addPhone->error;
            echo json_encode(array(
                'message' => 'Registration failed',
                'error' => $error
            ));
            $con->rollback();
            $con->close();
            die();
        }

        //Add other one
        $phoneID = getToken(6);
        $phoneNum = $altOne;
        if (!$addPhone->execute()) {
            $error = $addPhone->error;
            echo json_encode(array(
                'message' => 'Registration failed',
                'error' => $error
            ));
            $con->rollback();
            $con->close();
            die();
        }

        //Add other two if its valid
        if (strlen($_POST["altTwo"]) > 5) {
            $phoneID = getToken(6);
            $phoneNum = $altTwo;
            if (!$addPhone->execute()) {
                $error = $addPhone->error;
                echo json_encode(array(
                    'message' => 'Registration failed',
                    'error' => $error
                ));
                $con->rollback();
                $con->close();
                die();
            }
        }

        $addPhone->close();

        /* END OF ADDING PHONE NUMBERS */


        /* BEGIN ADDING USER DETAILS */

        $addDetails = $con->prepare("INSERT INTO user_details (uid,name, phoneNum, otherOne, otherTwo) VALUES (?,?,?,?,?)");
        $addDetails->bind_param('sssss', $userId, $fullName, $phoneNum, $otherOne, $otherTwo);
        $userId = $id;
        $fullName = $fName;
        $phoneNum = $main;
        $otherOne = $altOne;
        $otherTwo = $altTwo;
        if (!$addDetails->execute()) {
            $error = $addDetails->error;
            if ($error == "Duplicate entry '$phoneNum' for key 'phoneNum'") {
                echo json_encode(array(
                    'message' => 'Registration failed',
                    'error' => "$phoneNum is already registered"
                ));
            } else {
                echo json_encode(array(
                    'message' => 'Registration failed',
                    'error' => $error
                ));
            }
            $con->rollback();
            $con->close();
            die();
        }

        $addDetails->close();

        /* END OF ADDING USER DETAILS */

        /* BEGIN PHONE-USER MAPPING */

        /* Checking if the number is already registered as OTHER number by another user */
        $checkForExisting = $con->prepare("SELECT * FROM user_phone_mapping WHERE phoneNum=? AND ty_id=?");
        $checkForExisting->bind_param('ss', $phoneNum, $type);

        $phoneNum = $altOne;
        $type = 'OTHER';
        if ($checkForExisting->execute()) {
            $checkForExisting->store_result();
            if ($checkForExisting->num_rows >= 1) {
                echo json_encode(array(
                    'message' => 'Registration failed',
                    'error' => "$phoneNum is registered to another account"
                ));
                $con->rollback();
                $con->close();
                die();
            }
        }

        if (strlen($altTwo) > 5) {
            $phoneNum = $altTwo;
            $type = 'OTHER';
            if ($checkForExisting->execute()) {
                $checkForExisting->store_result();
                if ($checkForExisting->num_rows >= 1) {
                    echo json_encode(array(
                        'message' => 'Registration failed',
                        'error' => "$phoneNum is registered to another account"
                    ));
                    $con->rollback();
                    $con->close();
                    die();
                }
            }
        }
        $checkForExisting->close();

        /* End of checking*/

        $doPhoneMapping = $con->prepare("INSERT IGNORE INTO user_phone_mapping (uid, phoneNum,ty_id) VALUES (?,?,?)");
        $doPhoneMapping->bind_param('sss', $userId, $phoneNum, $type);
        $userId = $id;
        $phoneNum = $main;
        $type = 'MAIN';
        if (!$doPhoneMapping->execute()) {
            $error = $doPhoneMapping->error;
            echo json_encode(array(
                'message' => 'Registration failed',
                'error' => $error
            ));
            $con->rollback();
            $con->close();
            die();
        }

        $userId = $id;
        $phoneNum = $altOne;
        $type = 'OTHER';
        if (!$doPhoneMapping->execute()) {
            $error = $doPhoneMapping->error;
            echo json_encode(array(
                'message' => 'Registration failed',
                'error' => $error
            ));
            $con->rollback();
            $con->close();
            die();
        }

        if (strlen($altTwo) > 5) {
            $userId = $id;
            $phoneNum = $altTwo;
            $type = 'OTHER';
            if (!$doPhoneMapping->execute()) {
                $error = $doPhoneMapping->error;
                echo json_encode(array(
                    'message' => 'Registration failed',
                    'error' => $error
                ));
                $con->rollback();
                $con->close();
                die();
            }
        }

        $doPhoneMapping->close();

        /* END PHONE-USER MAPPING */


        /* BEGIN ADDING AVATAR DETAILS */

        if (strlen($image64) < 20) {
            echo json_encode(array(
                'message' => 'Registration successful',
                'userID' => $id,
                'avatarOk' => false
            ));
        } else {
            $filename = "avatars/$id.jpg";
            $filepath = "http://flashesvs.com/backend/tconnect/functions/$filename";

            file_put_contents($filename, base64_decode($image64, true));

            $addAvatar = $con->prepare("INSERT INTO avatar_details (avtr_id, avatar_resource) VALUES (?,?)");
            $addAvatar->bind_param('ss', $userId, $path);

            $userId = $id;
            $path = $filepath;
            if ($addAvatar->execute()) {
                echo json_encode(array(
                    'message' => 'Registration successful',
                    'userID' => $id,
                    'avatarOk' => true
                ));
            } else {
                $error = $addAvatar->error;
                echo json_encode(array(
                    'message' => 'Registration failed',
                    'error' => $error
                ));
                $con->rollback();
                $con->close();
                die();
            }
        }

        /* END ADDING AVATAR DETAILS */

        $con->commit();
        //$con->rollback();
        $con->close();

    } else {
        echo json_encode(array('error' => 'Invalid API Key'));
        die();
    }

}