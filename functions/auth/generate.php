<?php
/**
 * Created by PhpStorm.
 * User: Wickerman
 * Date: 2/7/2017
 * Time: 10:10 PM
 */

function secure_rand($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min;
    $log = ceil(log($range, 2));
    $bytes = (int)($log / 8) + 1;
    $bit = (int)$log + 1;
    $filter = (int)(1 << $bit) - 1;

    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter;
    } while ($rnd > $range);

    return $min + $rnd;
}

function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet .= "1234567890";
    $max = strlen($codeAlphabet);

    for ($i = 0; $i < $length; $i++) {
        $token .= $codeAlphabet[secure_rand(0, $max - 1)];
    }

    return $token;
}

//echo getToken(64);