<?php
/**
 * Created by PhpStorm.
 * User: Wickerman
 * Date: 5/10/2016
 * Time: 06:48 PM
 */
//Defining Constants
define('HOST', 'localhost');
define('USER', 'root');
define('PASS', '');
define('DB', 'tconnect-v2-beta');

//Connecting to Database
$con = mysqli_connect(HOST, USER, PASS, DB) or die(json_encode(array(
    'Error' => 'Failed to connect to database'
))); //Defining Constants