<?php
/**
 * Created by PhpStorm.
 * User: Wickerman
 * Date: 2/12/2017
 * Time: 1:58 PM
 */

ini_set('display_errors', 'Off');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $apikey = urldecode($_POST["apiKey"]);
    $localAPIKey = file_get_contents("files/api_key_app");

    if ($apikey == $localAPIKey) {

        require_once 'auth/generate.php';
        require_once 'auth/db-connect.php';

        $id = $_POST["uid"];
        $pOne = $_POST["JSON"];
        $phoneNumbers = json_decode($pOne, true);

        mysqli_autocommit($con, false);

        foreach ($phoneNumbers['contacts'] as $key => $value) {
            $phoneID = getToken(6);
            $addPhone = "INSERT INTO phone_numbers (ph_id, number) VALUES ('$phoneID','$value') ON DUPLICATE KEY UPDATE phone_numbers.number=values(number)";
            $doPhoneMapping = "INSERT IGNORE INTO user_phone_mapping (uid, phoneNum, ty_id) VALUES ('$id','$value','HAS')";

            if (mysqli_query($con, $addPhone)) {
                if (!mysqli_query($con, $doPhoneMapping)) {
                    $error = mysqli_error($con);
                    mysqli_rollback($con);
                    echo json_encode(array(
                        'message' => 'Adding failed',
                        'error' => $error
                    ));
                }
            } else {
                $error = mysqli_error($con);
                mysqli_rollback($con);
                echo json_encode(array(
                    'message' => 'Adding failed',
                    'error' => $error
                ));
            }
        }

        mysqli_commit($con);
        mysqli_close($con);

        echo json_encode(array(
            'message' => 'Initialization complete',
            'otherOk' => true
        ));


    } else {
        echo json_encode(array('error' => 'Invalid API Key'));
        die();
    }
}