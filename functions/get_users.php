<?php
/**
 * Created by PhpStorm.
 * User: Wickerman
 * Date: 2/8/2017
 * Time: 1:47 PM
 */

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    require_once 'auth/db-connect.php';

    $apikey = $_POST["apiKey"];
    $localAPIKey = file_get_contents("files/api_key_app");

    if ($localAPIKey == $apikey) {
        $uid = $_POST["uid"];

        $sql = $con->prepare("SELECT uid,name,phoneNum,otherOne,otherTwo,privacy,avatar_resource 
                FROM `user_details` LEFT OUTER JOIN `avatar_details` ON (`user_details`.`uid`=`avatar_details`.`avtr_id`)
                WHERE `status`= ? AND `uid` IN ( SELECT `uid`  FROM `user_phone_mapping` 
                WHERE `ty_id` = ? AND `phoneNum` IN 
                ( SELECT `phoneNum` FROM `user_phone_mapping` WHERE `ty_id`= ? AND `uid`= ?) )");

        $sql->bind_param("ssss", $status, $typeOne, $typeTwo, $uid);
        $typeOne = "OTHER";
        $status = "ACTIVE";
        $typeTwo = "HAS";

        $users = array();
        if ($sql->execute()) {
            $sql->store_result();
            $sql->bind_result($i, $n, $p, $o1, $o2, $pr, $av);

            while ($sql->fetch()) {
                array_push($users, array(
                    "id" => $i,
                    "name" => $n,
                    "phoneNum" => $p,
                    "otherOne" => $o1,
                    "otherTwo" => $o2,
                    "privacy" => $pr,
                    "avatar" => $av
                ));
            }
        }
        echo json_encode(array('results' => $users));

        $con->close();
    } else {
        echo json_encode(array('Error' => 'Invalid API Key'));
        die();
    }
}